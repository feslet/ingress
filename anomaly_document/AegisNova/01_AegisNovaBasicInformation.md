# 1回目　アノマリー基本情報
## 公式チケットの入手
* [Ingress XM Anomaly - Aegis Nova - Tokyo](http://events.ingress.com/xmanomalies/aegis-nova/Tokyo)
* メールに添付されているPDFをプリントアウトして持参すること！

## ■日程確認

### July 15th (金)
#### 16:00 - 21:00*
* サポーターキット受け取り及び事前受付 [Supporter Kit Pickup & Pre-registration]
  * お台場特別会場

### July 16th (土)  Aegis Nova XM Anomaly
#### 8:00 - 12:00*
* 受付及びサポーターキット受け取り [Registration + Supporter Kit Pickup]
  * お台場 特別会場
* [Aegis Nova Anomalies - July 16 2016 - AegisNova-July-16-Anomalies-Tokyo-JP.pdf](http://investigate.ingress.com/wp-content/uploads/2016/07/AegisNova-July-16-Anomalies-Tokyo-JP.pdf)

#### 18:00*
* アフターパーティー入場開始(有料キット購入者) [Cross Faction Afterparty Opens (Paid Pack)]
  * お台場 特別会場

#### 18:30*
* アフターパーティー入場開始 (無料参加者) [Cross Faction Afterparty Opens (FREE)]
  * お台場 特別会場

#### 19:30*
* アフターパーティー開始
* お台場 特別会場

#### 20:30*
* 終了 End

#### 22：00 - 24 : 00
* [Ingress AegisNova Tokyo Resistance After Party (RESアフターパーティー) | Peatix](http://resistance-aap.peatix.com/)
  * 本部(IRT:Ingress Resistance Tokyo)主催?
>   内容：Resistance限定アフターパーティー
>   日時：2016年7月16日（土）22：00 - 24 : 00
>   場所：ベノア（BENOA） 銀座店 B3F
>   　　　東京都中央区銀座6-13-16 銀座ウォールビル B3F
>   　　　(Google Map) https://goo.gl/maps/fvQH9ERjM3v
>   URL：http://www.benoa.jp/ginza/index.html
>   参加費：2,500円（フリードリンクのみ。フードなし）
>   ・オプションで、+1,500円で4 : 00までオールナイトにできます。
>   （オールナイトチケットのみ購入して、0 : 00 - 4 : 00の利用も可能です。二次会にいかがでしょう）
>   ・すぐ近くにファミレスや飲食店、コンビニがあります。深夜の出入りは自由です。
>   ※お預かりした個人情報は適切に取り扱い、二次利用はいたしません。
>   ※未成年者の飲酒およびオールナイト利用はお断りしております。


#### 21：00～24：00（3時間）受付は20:00〜
* [IRK presents AEGIS NOVA TOKYO After After Party（AAP) @ shinbashi | Peatix](http://peatix.com/event/179824)
  * IRK主催
> 日時：
> 2015年7月16日 (土) 時間：21：00～24：00（3時間）受付は20:00〜
>
> 場所：Dart,sUP新橋店
> 【住所】 東京都港区新橋2-15-18 プラザT2ビル7Ｆ
> 【MAP】 https://goo.gl/maps/eTakPt2fAv42
> 【電話】 03-6205-4248
> 【最寄駅】 新橋駅（SL広場側)
>
> 参加費：4000円

## ■RES組織概要

### 組織概要
![体制図(BPJより。閲覧のためにはBPJログイン必要)](https://files.slack.com/files-pri/T0BR5706L-F15NA2L3U/_________2.png)

* 運営本部と作戦本部あり。
* 運営本部のリーダーは FL(Faction Leader)でもある [Ruin Dig - Google+](https://plus.google.com/+RuinDig/posts)
* 作戦本部のリーダーは GM(General Manager)の [michael0000 N.K - Google+](https://plus.google.com/+michael0000NK/posts)

#### 運営本部
* あまり気にしないで良いが裏方で頑張ってらっしゃいます

#### 作戦本部

##### 広報

##### 海外対応

##### バックオフィス(BO)

##### グラウンド
* [Z aimas - Google+](https://plus.google.com/+KentaSatozaimas/posts)

##### 広域

### チーム編成

### コミュニティ
#### [[BlueBase]AegisNova Resistance Tokyo - コミュニティ - Google+](https://plus.google.com/communities/107038185199975356571)
  * 管理者：AegisNovaTokyo 東京作戦室本部的なところ
  * 参加者：AegisNovaTokyo 参加者(現地、遠隔、広域 問わず)
  * something else...

#### [[Leaders]AegisNova Resistance Tokyo - コミュニティ - Google+](https://plus.google.com/communities/100807278702032000847)
* 管理者：AegisNovaTokyo 東京作戦室本部的なところ
* 参加者：AegisNovaTokyo 参加チームのリーダー
* something else...

#### [[Operators] AegisNova Resistance Tokyo - コミュニティ - Google+](https://plus.google.com/communities/112353406303217583388)
* 管理者：AegisNovaTokyo 東京作戦室本部的なところ
* 参加者：AegisNovaTokyo 参加のオペレーター希望者
* something else...

#### [[GORUCK] Aegis Nova Resistance Tokyo - コミュニティ - Google+](https://plus.google.com/communities/100674942307608057689)
* 管理者：AegisNovaTokyo 東京作戦室本部的なところ
* 参加者：AegisNovaTokyo 参加の GORUCK 希望者
* something else...

#### [\*RES Visitors\* Recapture Tokyo -AEGIS NOVA - コミュニティ - Google+](https://plus.google.com/communities/108078474180338473664)
* 管理者：AegisNovaTokyo 東京作戦室本部的なところ
* 参加者：AegisNovaTokyo 参加の Visitors
* something else...

#### [【イージスノバ東京】全国遠征団【FERT】 - コミュニティ - Google+](https://plus.google.com/communities/109815655165443468184)
* 管理者：IRK(Ingress Resistance Kansai)
* 参加者：IRK主導の遠征団に帯同するチーム、ユニット、個人？
* something else...

### 資料類
#### IRK 資料
* [flash shards局地戦術レポート.pdf のコピー - Google ドライブ](https://drive.google.com/file/d/0B_nvOQYox8X3QUhWV0IzQWhLQzA/view)

#### チーム資料
* [aegis tokyo 修羅の国からこんにちは - Google スプレッドシート](https://docs.google.com/spreadsheets/d/1_QsRgI4xGV7W0CI-d-rlK3iE41SiuqFH5zVfq6ArDO4/edit#gid=612793378)
* [AegisNova 修羅の国チーム - Google スプレッドシート](https://docs.google.com/spreadsheets/d/1687MuCmEe4yTcUczge7xgQ0dTBMjhhXl6c_EeOYJQtc/edit#gid=0)

## ■前日/当日の行動予定
### 前日
* 前日入りできる人は、サポーターキット受け取り及び受付は事前がオススメ(金曜 16-21時)
* [お台場特別会場は「お台場 シンボルプロムナード公園 夢の広場」？　イージスノヴァ(Aegis Nova)便利帳 | Ingress(イングレス)速報 より](http://ingressblog.jp/aegis-nova-schedule/)
* くどいけど PDF 印刷して持っていくように！！
* ウェルカムパーティ等は不明
* 前日FF等の企画も不明
* 充電は忘れないように！（特にモバイルバッテリーの充電を忘れがち。時間がかかるので就寝中に充電できるようにしておく）

### 当日
* 事前受付が出来ない人は当日受付。(土曜 8-12時)
  * クラスタ戦、シャード戦に向けて事前の作業があるので なる早 で受付を済ませる方が良い
* 受付が済んだらグループで集合。時間と場所は T.B.D (具体的なクラスタマップが出てから。本番の一週間前ぐらい。[Declassified AEGIS NOVA Anomaly Intel and Maps for June 25 2016](http://investigate.ingress.com/2016/06/18/declassified-aegis-nova-anomaly-intel-and-maps-for-june-25-2016/))

## ■ルールをざっくりと（2回目で掘り下げる）
### 過去資料
* DAY1(英語): [Declassified AEGIS NOVA Anomaly Intel](http://investigate.ingress.com/2016/05/21/declassified-aegis-nova-anomaly-intel/)
* DAY2(英語): [Declassified AEGIS NOVA Anomaly Intel and Maps for June 25 2016](http://investigate.ingress.com/2016/06/18/declassified-aegis-nova-anomaly-intel-and-maps-for-june-25-2016/)
* ハイブリッド戦。[Aegis Nova Anomalies - June 25 2016 - AegisNova-June-25-Anomalies-Sydney.pdf](http://investigate.ingress.com/wp-content/uploads/2016/06/AegisNova-June-25-Anomalies-Sydney.pdf)
> ANOMALY TYPE: Hybrid (Cluster Battle + Flash Shards)
* [ざっくりした日本語訳(中田吉法さん)](https://plus.google.com/u/0/106896671642633446443/posts/W5pPn89tZ8B)

### タイムテーブル
* [AEGIS NOVA TOKYOタイムテーブル.pdf - Google ドライブ](https://drive.google.com/file/d/0BxWWSrLRJu5OWmFuSEUyZlZlT00/view)
  * P2. 全体→ゾーン→クラスタ の順。DAY1, 2 でゾーンの概念は無かった(多分)。過去東京で開催されたDARSANA東京では4つのゾーンに分かれていた。（参加者が桁違い）
  * P3. クラスタ戦とシャード戦の計測が同時に行われることはないが、12:30 の RES/ENL ターゲット(シャードのゴール) 出現後は休む暇なし
  * P3. 過去のアノマリーでは事前にオーナメントと呼ばれる光が出現していたが、AegisNovaシリーズでは各メジャメントの1時間前に点灯。段階的に点灯したり、計測が始まってから点灯したという報告もあり？
  * P3. シャード戦とクラスタ戦が1セット、4セット行われる。シャード戦が先。
  * シャード発生時のイメージ(シドニーの例。発生した時点で繋がっているリンクがあれば防衛。繋がってなければ繋げに行く。)
  ![シャード発生位置のイメージ](https://bytebucket.org/feslet/ingress/raw/47c3a1eee987f1202922a98727e893ac811e4bb6/anomaly_document/AegisNova/01_41_Sydney_M4_0.png)

## ■リアル装備、インベントリについて軽く
### リアル装備
* [装備 シート参照　→ aegis tokyo 修羅の国からこんにちは](https://docs.google.com/spreadsheets/d/1_QsRgI4xGV7W0CI-d-rlK3iE41SiuqFH5zVfq6ArDO4/edit#gid=612793378)

#### ハード
 * スキャナ
   * 端末1
   * キャリア1
   * 端末2
   * キャリア2
   * バッテリー1
   * バッテリー2
   * ケーブル本数
 * 通信
   * イヤホン

#### ソフト
 * 通信
   * Zello [Zello. Live conversations.](https://zello.com/)
   * TS3(有料) [Downloads - TeamSpeak](https://www.teamspeak.com/downloads)
 * 位置共有
   * Glympse [Get Glympse: Glympse](https://www.glympse.com/get-glympse)
   * レスラー(謎) [[iOS](https://itunes.apple.com/jp/app/reswue-client/id971201245?mt=8)][[Android](https://play.google.com/store/apps/details?id=net.irde.ops.agentclient&hl=ja)]

#### 服装類
 * 暑さ対策
   * 帽子(強く推奨)
   * タオル
   * 水
   * 日焼け止め
   * リップ(日焼け対策)
 * 虫対策
   * 虫除け
 * 雨対策
   * ポンチョ
   * ジップロック等
 * ケガ対策
   * 靴
 * 威圧
   * サングラス
 * その他
   * 流せるティッシュ

### インベントリデザイン(インベントリ構成)
* [インベントリ シート参照　→ aegis tokyo 修羅の国からこんにちは](https://docs.google.com/spreadsheets/d/1_QsRgI4xGV7W0CI-d-rlK3iE41SiuqFH5zVfq6ArDO4/edit#gid=612793378)
  * 初参加者向け、バランスの目標値を設定(本チームの独自設定。自立と共闘)。経験や地元のファーム状況等で調整を。
  * 足りなければ早めにヘルプを要請する。
  * 処分しづらいMODなどのアイテムは知人がいれば預ける。地元の鍵は整理も兼ねて捨てるのがいいかも。

# 2回目　ルールについて
## そもそもアノマリーって何が目的？
* [Aegis Nova Scoreboard](http://investigate.ingress.com/aegis-nova-scoreboard/)
* DAY1(May 28, 2016), DAY2(June 25, 2016) の Primary site : 2 point, Satelite site : 1 point
* DAY3(July 16, 2016, 東京) は 5 point!
* DAY2 を終えて Enlightened 17-7 Resistance なので逆転の目はない、、けど一矢報いたい！！

## ハイブリッドって何？
* ハイブリッド戦。[Aegis Nova Anomalies - June 25 2016 - AegisNova-June-25-Anomalies-Sydney.pdf](http://investigate.ingress.com/wp-content/uploads/2016/06/AegisNova-June-25-Anomalies-Sydney.pdf)
> ANOMALY TYPE: Hybrid (Cluster Battle + Flash Shards)
* [ざっくりした日本語訳(中田吉法さん)](https://plus.google.com/u/0/106896671642633446443/posts/W5pPn89tZ8B)

## クラスタ戦って何？
* ある瞬間の、対象ポータルのオーナー陣営(青か緑か)を測定、個数から点数が導き出される。リンクやCFがあればボーナス
* 四回戦勝負
* 一回戦の場所はここ、二回戦は、、のように、クラスタという範囲が NIA によって定義され、その中で計測が行われる
* 一回戦 14:00-14:10、、、四回戦 17:00-17:10 のように、10分間のある瞬間の状態を NIA が判定する
* よって対象ポータル(クラスタ1〜4それぞれに出現、計測開始1時間前にならないと分からない[諸説あり])を自陣営の色で10分間キープできればよし
* 壮絶なわんこ合戦となる。処理が遅延し、バースターを撃ってもなかなかダメージを与えられない(ぐるぐる回って破裂？しない)状態となる
* [Tokyo Anomaly Intel: Cluster Images (7/5 22:50 頃？発表)](http://investigate.ingress.com/2016/07/04/tokyo-anomaly-intel-cluster-images/)
  * クラスタ1 新宿、上野秋葉原、両国曳舟
  * クラスタ2 渋谷、銀座日本橋、清澄白河
  * クラスタ3 恵比寿目黒、六本木芝公園、門仲
  * クラスタ4 豊洲、築地、お台場
* [AEGIS-NOVA TOKYO CLUSTER MAP([ANRT] Operators HOより)](https://www.google.com/maps/d/edit?mid=1RQ74wUd-PQVupV0FzZE3Os4DF84)
* [ピカピカ点滅する自分用メモ。 #tokyo迷子 #見てもどこだかわかんない  ﻿](https://plus.google.com/+sheelduo/posts/2qUPNPyNkuU?pid=6303846318967343938&oid=100533495227472787702)

## シャード戦って何？
* RES/ENL それぞれに「ターゲット」と呼ばれるゴール(ポータル)が複数箇所設定される(個数は開催地ごとに異なる)。ゴールは、当日 12:30 頃判明する傾向にある？
* ゴールに設定されたポータルと、シャードと呼ばれる浮遊物(個数は開催地ごとに異なる)が発生したポータルをリンクで繋ぐ
* シャードは、ある時間になるとリンクを伝って移動する(ゴールと繋がってればゴールする)。ただし、リンクが繋がっているポータル同士が L4 以上である場合に限る。
* 暗号を解いたり(デコード班と呼ばれる人たちがいます)、 GORUCK(前夜に行われる GORUCK Stealth) によってヒントがもらえる？（詳しく知らない）
* あとはタイムテーブル見てくださいｗ

### 過去資料
* DAY1(英語): [Declassified AEGIS NOVA Anomaly Intel](http://investigate.ingress.com/2016/05/21/declassified-aegis-nova-anomaly-intel/)
* DAY2(英語): [Declassified AEGIS NOVA Anomaly Intel and Maps for June 25 2016](http://investigate.ingress.com/2016/06/18/declassified-aegis-nova-anomaly-intel-and-maps-for-june-25-2016/)

### タイムテーブル
* [AEGIS NOVA TOKYOタイムテーブル.pdf - Google ドライブ](https://drive.google.com/file/d/0BxWWSrLRJu5OWmFuSEUyZlZlT00/view)
  * P2. 全体→ゾーン→クラスタ の順。DAY1, 2 でゾーンの概念は無かった(多分)。過去東京で開催されたDARSANA東京では4つのゾーンに分かれていた。（参加者が桁違い）
  * P3. クラスタ戦とシャード戦のイベント(クラスタ戦計測、シャードジャンプ等)が同時に発生することはないが交互に発生するため、12:30 の RES/ENL ターゲット(シャードのゴール) 出現後は休む暇なし
  * P3. 過去のアノマリーでは事前にオーナメントと呼ばれる光が出現していたが、AegisNovaシリーズでは各メジャメントの1時間前に点灯。段階的に点灯したり、計測が始まってから点灯したという報告もあり？
  * P3. シャード戦とクラスタ戦が1セット、4セット行われる。シャード戦が先。
  * シャード発生時のイメージ(シドニーの例。発生した時点で繋がっているリンクがあれば防衛。繋がってなければ繋げに行く。)
  ![シャード発生位置のイメージ](https://bytebucket.org/feslet/ingress/raw/47c3a1eee987f1202922a98727e893ac811e4bb6/anomaly_document/AegisNova/01_41_Sydney_M4_0.png)

## レスラー(RESWUE)
RESWUE Client
Android: https://play.google.com/store/apps/details?id=net.irde.ops.agentclient&hl=en
iOS: https://itunes.apple.com/us/app/reswue-client/id971201245

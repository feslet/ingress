# 3回目　オペレータとフィールダーの役割について
## 当日の流れを確認(シャード出現、クラスタ戦、アフターパーティ)
### [■ ルール発表：Aegis Nova Anomalies](http://investigate.ingress.com/wp-content/uploads/2016/07/AegisNova-July-16-Anomalies-Tokyo-JP.pdf)

### [■ mufudoujin 無風道人](https://plus.google.com/111448245885651130309/posts) 氏 考察
* [今回のアノマリーの戦い方についての試論です。議論のたたき台になれば幸いです ●シャードとターゲットの分布予想 ・Anomaly…](https://plus.google.com/u/0/111448245885651130309/posts/N1v787copRS)
* [今回のアノマリーの戦い方についての試論（クラスタ戦編）です。議論のたたき台になれば幸いです ●基本 ・クラスタ戦対象ポータルは毎回約333…](https://plus.google.com/111448245885651130309/posts/QNuH5TAjkMt)
* [AegisNovaTokyoルール解説.pdf](https://drive.google.com/file/d/0ByOxjsymX0_nQlN0amtBM1gyV2c/view)

## フィールダーのお仕事
* 状況に応じて自分、チームのやるべきことを判断する
* リーダー、オペレーターと相談してその行動を確認する
* スキャナ、マップでの見え方
  - [ビーコン、フラッカー、シャード、それぞれintel、もしくはスキャナーで見分けられますかー？…](https://plus.google.com/102668513708874343708/posts/QJuX7mXWv9p)
  - [浜松の時に撮ったスクリーンショット(daiku)](https://drive.google.com/folderview?id=0B-XWFdmf4DKHTVdXTkp5cF9qTzA&usp=sharing)
* QA
  - [Q.「アノマリーの当日、ターゲット出現の1時間ほど前までは何をして過ごしたら良いか？」 …](https://plus.google.com/+YATAGARASS/posts/ADc3iAaTMYs)
  - [Q.「RESのターゲットポータルからアウトリンクは引いて良いのか？」 勉強会で出た質問ですが、周知したい内容なので本部見解として回答いたします。 …](https://plus.google.com/+YATAGARASS/posts/32mSTttW22v)
  - [今回、フラッシュシャード戦を想定した時に、多くのAGの皆さんが ターゲットなどに対するリンク引き…](https://plus.google.com/+KenAKASHI/posts/ft3KRmUVLFY)
  - [Q.ターゲットに接続しないリンクは作成して良いのか？ おそらく、現場で直面するであろう疑問にお答えします。 …](https://plus.google.com/+YATAGARASS/posts/Lz6EzJL5Mdc)
  - [Q.「ENLのターゲットポータルには何のModを入れれば良いですか？」 …](https://plus.google.com/+KenAKASHI/posts/JDECvAsFEMv)
* 資料
  - [グラウンドを制するために](https://docs.google.com/presentation/d/10GaVGsRKt_FnabKXyoO5ByxfeaQk6am0rZM1l7q5-HY/edit#slide=id.g144c872e92_0_67)


## オペレータのお仕事
* Intel Map 等から得られる情報(周辺ポータル情報、取れた場合は免疫情報)によりフィールダーの行動を判断する補助を行う
* タイムキーパー(予定された次の行動を伝える)

## パターン別行動の確認

## 資料
### リーダー向け
* [TLのための準備ガイド.pdf](https://drive.google.com/file/d/0BwUkEw0vfSffMFd1RGIzQXNoVlE/view)
* [はじめてのRESWUE.pdf](https://drive.google.com/file/d/0B8nLBNJ4TdS0bEV4LWJpc1c1WG8/view)
* [現地行動パターン](https://docs.google.com/presentation/d/1QoVzBSlKNgbhjmoeWeUB72hVTHxFlnCz-bI189JkcCw/edit#slide=id.g151611a407_0_57)
* [【当日の戦い方マニュアル「案！」（チームリーダ向け）の展開】 お疲れ様です。 他の皆様の資料が素敵すぎて、…](https://plus.google.com/+michael0000NK/posts/anhq1sDnevH)
